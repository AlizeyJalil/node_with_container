FROM node:slim

COPY nodeapp /opt/

ENTRYPOINT nodejs /opt/app.js
